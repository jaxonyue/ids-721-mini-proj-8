# IDS 721 Mini Proj 8 [![pipeline status](https://gitlab.com/jaxonyue/ids-721-mini-proj-8/badges/main/pipeline.svg)](https://gitlab.com/jaxonyue/ids-721-mini-proj-8/-/commits/main)

## Overview
* This repository includes the components for **Mini-Project 8 - Rust Command-Line Tool with Testing**

## Goal
* Rust command-line tool
* Data ingestion/processing
* Unit tests

## My Lambda Function
* My script is written in Rust and uses the Cargo Lambda framework. It contains two functions:
  * `uppercase` - This function takes a string as input and returns the string in uppercase
  * `lowercase` - This function takes a string as input and returns the string in lowercase

## Screenshot of the CLI Tool Sample Output
![CLI_Tool](/uploads/8b7b5e071a098ecc67077327a79b7757/CLI_Tool.png)

## Screenshot of the Test Report
![Tests](/uploads/7be4f4f02fcb05a6466afb7692b657fe/Tests.png)

## Key Steps
1. Install Rust and Cargo Lambda per the instructions in the [Cargo Lambda documentation](https://www.cargo-lambda.info/guide/installation.html) and [Rust documentation](https://www.rust-lang.org/tools/install)
2. Create a new Rust project using `cargo lambda new <project_name>`
3. Write the code for the Lambda Function in the `src/main.rs` file, using the parsed command line arguments to control the flow
4. Add the necessary dependencies to the `Cargo.toml` file
5. Build the project using `cargo lambda build --release`
6. Find the compiled program by running `cd target/release`
7. Run the program with the desired arguments `./<project_name> <function_name> <input_string>`
8. For my project, an example command would be `./proj uppercase "hello world"`
9. Write unit tests for the functions by adding a `tests` module to the `src/main.rs` file
10. Run the tests using `cargo lambda test`
