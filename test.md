running 10 tests
test tests::test_to_lowercase_empty_string ... ok
test tests::test_to_lowercase ... ok
test tests::test_to_lowercase_mixed_case ... ok
test tests::test_to_lowercase_symbols ... ok
test tests::test_to_lowercase_numbers ... ok
test tests::test_to_uppercase ... ok
test tests::test_to_uppercase_empty_string ... ok
test tests::test_to_uppercase_mixed_case ... ok
test tests::test_to_uppercase_numbers ... ok
test tests::test_to_uppercase_symbols ... ok

test result: ok. 10 passed; 0 failed; 0 ignored; 0 measured; 0 filtered out; finished in 0.00s