use std::env;

/// Converts an input string to all uppercase letters.
fn to_uppercase(input: &str) -> String {
    input.to_uppercase()
}

/// Converts an input string to all lowercase letters.
fn to_lowercase(input: &str) -> String {
    input.to_lowercase()
}

fn main() {
    // Collect arguments from the command line
    let args: Vec<String> = env::args().collect();

    // Basic command line argument parsing
    // Expecting the program to be called with two arguments: the command and the message
    // Example: cargo run uppercase "Hello, World!"
    if args.len() != 3 {
        eprintln!("Usage: {} <command> <message>", args[0]);
        eprintln!("Commands: uppercase, lowercase");
        std::process::exit(1);
    }

    let command = &args[1];
    let message = &args[2];

    let result = match command.as_str() {
        "uppercase" => to_uppercase(message),
        "lowercase" => to_lowercase(message),
        _ => {
            eprintln!("Invalid command: {}", command);
            std::process::exit(1);
        }
    };

    println!("{}", result);
}

#[cfg(test)]
mod tests {
    use super::*;

    #[test]
    fn test_to_uppercase() {
        let input = "hello, world!";
        let expected = "HELLO, WORLD!";
        assert_eq!(to_uppercase(input), expected);
    }

    #[test]
    fn test_to_lowercase() {
        let input = "HELLO, WORLD!";
        let expected = "hello, world!";
        assert_eq!(to_lowercase(input), expected);
    }

    #[test]
    fn test_to_uppercase_empty_string() {
        let input = "";
        let expected = "";
        assert_eq!(to_uppercase(input), expected);
    }

    #[test]
    fn test_to_lowercase_empty_string() {
        let input = "";
        let expected = "";
        assert_eq!(to_lowercase(input), expected);
    }

    #[test]
    fn test_to_uppercase_numbers() {
        let input = "12345";
        let expected = "12345";
        assert_eq!(to_uppercase(input), expected);
    }

    #[test]
    fn test_to_lowercase_numbers() {
        let input = "12345";
        let expected = "12345";
        assert_eq!(to_lowercase(input), expected);
    }

    #[test]
    fn test_to_uppercase_symbols() {
        let input = "!@#$%^&*()";
        let expected = "!@#$%^&*()";
        assert_eq!(to_uppercase(input), expected);
    }

    #[test]
    fn test_to_lowercase_symbols() {
        let input = "!@#$%^&*()";
        let expected = "!@#$%^&*()";
        assert_eq!(to_lowercase(input), expected);
    }

    #[test]
    fn test_to_uppercase_mixed_case() {
        let input = "HeLlO, WoRlD!";
        let expected = "HELLO, WORLD!";
        assert_eq!(to_uppercase(input), expected);
    }

    #[test]
    fn test_to_lowercase_mixed_case() {
        let input = "HeLlO, WoRlD!";
        let expected = "hello, world!";
        assert_eq!(to_lowercase(input), expected);
    }


    // You can add more tests here for edge cases, like empty strings, strings with numbers, symbols, etc.
}
